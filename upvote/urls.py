from django.urls import path

from . import views

app_name = 'upvote'
urlpatterns = [
	path('', views.index, name='index'),
]
