from django.db import models
from people.models import User
from datetime import datetime

class Vote(models.Model):
    creator = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="vote_creator")
    description = models.CharField(max_length=1000)
    taskid = models.CharField(max_length=10)
    timestamp = models.DateTimeField('timestamp', default=datetime.now)
    voters = models.ManyToManyField(User, related_name="vote_voters")
    def __str__(self):
        return self.taskid + "-" + self.creator.username
