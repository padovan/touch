from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic.list import ListView
from .models import User
from praise.models import Praise
from feedback.models import Response
from tastypie.models import ApiKey
from feedback.views import responses_to_review 

@login_required
def index(request):
    context = {'users': User.objects.filter(is_active=True).order_by('first_name'), 'pending': responses_to_review(request.user).count() }
    return render(request, 'people/index.html', context)

@login_required
def profile(request, username):
    person = User.objects.filter(username=username)[0]
    kudos = Praise.objects.filter(receiver=person.id)
    feedback = Response.objects.filter(receiver=person.id,public=True,ready=True)
    if person == request.user or person.mentor == request.user:
        feedback = feedback.union(Response.objects.filter(receiver=person.id))

    context = {'user' : request.user, 'person' : person, 'kudos' : kudos, 'feedback' : feedback}
    return render(request, 'people/profile.html', context)

@login_required
def apikey(request):
    user = User.objects.filter(username=request.user.username)[0]

    apikey = ApiKey.objects.filter(user=user)[0]
    
    context = {'user' : user, 'apikey' : apikey }
    return render(request, 'people/apikey.html', context)

class MenteesView(ListView):
    model = User

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return User.objects.filter(mentor=self.request.user.id)

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, 'You do not have the permission.')
            return HttpResponseNotFound('<h1>Page not found</h1>')
        return super(MenteesView, self).dispatch(request,*args, **kwargs)