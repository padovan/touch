from .models import User
from social_core.pipeline.partial import partial

@partial
def fix_userinfo(strategy, response, *args, **kwargs):
	username = response['result']['userName']
	user = User.objects.filter(username = username)[0]
	return {
	    'user' : user,
	    'username' : username,
	    'uid' : response['result']['phid']
	    }
