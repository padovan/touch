"""touch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from praise.api import PraiseResource
from people.api import UserResource

user_resource = UserResource()
praise_resource = PraiseResource()

urlpatterns = [
    path('people/', include('people.urls')),
    path('feedback/', include('feedback.urls')),
    path('praise/', include('praise.urls')),
    path('upvote/', include('upvote.urls')),
    path('oauth2', include('social_django.urls', namespace='social')),
    path('api/v1/', include(user_resource.urls)),
    path('api/v1/', include(praise_resource.urls)),
    path('admin/', admin.site.urls),
    path('', include('people.urls')),
]
