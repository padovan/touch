from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseNotFound
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import logout
from django.contrib import messages
from django.views.generic.list import ListView
from django.utils import timezone
from django.core.mail import send_mail

from people.models import User, Department, Team
from .models import Response
from .forms import ResponseForm

#XXX not sure I like how we use is_curator as a context var to in the base.html menu
#XXX should receive a response as optional field
def user_is_curator(user, response = None):
    if not response:
        return Team.objects.filter(reviewer=user.id).exists()
    return Team.objects.filter(people__in=[response.giver], reviewer=user.id).filter(people__in=[response.receiver]).exists()

def responses_to_review(reviewer):
    responses = Response.objects.none()
    for response in Response.objects.filter(ready=False):
        if user_is_curator(reviewer, response):
            responses = responses.union(Response.objects.filter(pk=response.id))
    return responses

def send_feedback(response):
    message = '''
    Hello, {0}

    You have received a new feedback from {1}:

    "{2}"

    Go to https://touch.collabora.com/feedback/my/ to see all your feedback.

    Thank you for being part of our Feedback Culture! Give feedback to someone at https://touch.collabora.com/feedback.
    '''
    send_mail(
        'You received a new feedback!',
         message.format(response.receiver.first_name, response.giver.name(), response.text),
        'noreply@touch.collabora.com',
        [response.receiver.username + '@collabora.com'],
        fail_silently=False,
    )

@login_required
def index(request):
    giver = request.user
    teams = Team.objects.filter(people__in=[giver])

    receivers = User.objects.none()
    for team in teams:
        receivers = receivers.union(team.people.exclude(username=giver.username))

    if Department.objects.filter(lead=giver.id):
        receivers = receivers.union(User.objects.filter(department=giver.department).exclude(username=giver.username))
    
    others = User.objects.filter(is_active=True).exclude(username=giver.username).difference(receivers).order_by('first_name')

    context = {'receivers': receivers, 'giver' : giver, 'others': others }
    context['reviewer'] = user_is_curator(request.user)
    return render(request, 'feedback/index.html', context)

@login_required
def submit(request):
    user = request.user
    for item in request.POST:
        receiver = User.objects.filter(username=item)
        if receiver:
            anon = item + "_anon"
            if anon in request.POST:
                is_anon = True
            else:
                is_anon = False
            if request.POST[item] != '':
                ready = not Team.objects.filter(people__in=[user]).filter(people__in=[receiver[0]]).exists()
                response = Response(giver=user, receiver=receiver[0],text=request.POST[item], is_anon=is_anon, ready=ready, timestamp=timezone.now())
                response.save()
                if ready:
                    send_feedback(response)

    # should we have a try here
    context = {'giver' : user}
    return render(request, 'feedback/done.html', context)

class ReviewView(ListView):
    model = Response

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)
     
    def get_queryset(self):
        return responses_to_review(self.request.user)

    def post(self, request, *args, **kwargs):
        # save feedback edit
        if request.POST:
            form = ResponseForm(request.POST)
            if form.is_valid():
                #XXX is there a better way to get the reponse id?
                response = Response.objects.filter(pk=request.POST['id'])[0]
                response.text = form.cleaned_data['text']
                response.is_anon = form.cleaned_data['is_anon']
                response.save()
        
        return redirect('/feedback/review/')

    def dispatch(self, request, *args, **kwargs):
        if not user_is_curator(request.user):
            messages.error(request, 'You do not have the permission.')
            return HttpResponseNotFound('<h1>Page not found</h1>')
        
        # approve response #XXX this could be done better - in REST?
        if request.GET and 'id' in request.GET.keys():
            response = Response.objects.filter(pk=request.GET['id'])[0]
            response.ready = True
            response.save()
            send_feedback(response)

        return super(ReviewView, self).dispatch(request, *args, **kwargs)

class MyView(ListView):
    model = Response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
     
    def get_queryset(self):
        return Response.objects.filter(receiver=self.request.user.id,ready=True)

    def dispatch(self, request, *args, **kwargs):
        # approve response #XXX this could be done better - in REST?
        if request.GET and 'id' in request.GET.keys():
            response = Response.objects.filter(pk=request.GET['id'])[0]
            if (request.user == response.receiver):
                response.public = not response.public
                response.save()
        
        return super(MyView, self).dispatch(request, *args, **kwargs)

@login_required
def edit(request, id):
    response = Response.objects.filter(pk=id)[0]

    if not user_is_curator(request.user, response):
        return HttpResponseNotFound('<h1>Page not found</h1>')

    form = ResponseForm()
    form.fields['text'].initial = response.text
    form.fields['is_anon'].initial = response.is_anon

    context = {'response': response, 'form' : form}
    return render(request, 'feedback/edit.html', context)

