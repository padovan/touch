from django.contrib import admin
from .models import Response

class ResponseAdmin(admin.ModelAdmin):
    list_display = ('giver', 'receiver')
    search_fields = ('giver__username', 'receiver__username')

admin.site.register(Response, ResponseAdmin)
