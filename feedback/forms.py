from django import forms

#XXX use Meta here?
class ResponseForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea(), max_length=2000)
    is_anon = forms.BooleanField(required=False)
